﻿Feature: WeatherForecast
Simple Weather forecast API boilerplate

@tag1
Scenario: Get weather forecasts
	Given I am a client
	When I make a GET request to 'weatherforecast'
	Then the response status code is '200'
	And the result should be an array
