using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using SimpleAPIBoilerplate.Controllers;
using System.Net;
using System.Text.Json;

namespace SimpleAPIBoilerplate.Specs.StepDefinitions
{
    [Binding]
    public class WeatherForecastStepDefinitions
    {
        private WeatherForecastController _weatherForecastController = null!;
        private Task<IActionResult>? _request;
        private IActionResult? _result;

        [Given(@"I am a client")]
        public void GivenIAmAClient()
        {
            _weatherForecastController = new WeatherForecastController();
        }

        [When(@"I make a GET request to '(.*)'")]
        public void WhenIMakeAGETRequestTo(string endpoint)
        {
            _request = _weatherForecastController.Get();
            _result = _request.GetAwaiter().GetResult();
        }

        [Then(@"the response status code is '(.*)'")]
        public void ThenTheResponseStatusCodeIs(int statusCode)
        {
            var requestStatus = _result?.GetType()?.GetProperty("StatusCode")?.GetValue(_result, null);

            requestStatus.Should().Be(statusCode);
        }

        [Then(@"the result should be an array")]
        public void ThenTheResultShouldBeAnArray()
        {
            var requestValue = _result?.GetType()?.GetProperty("Value")?.GetValue(_result, null);

            requestValue.Should().BeOfType<WeatherForecast[]>();
        }
    }
}
